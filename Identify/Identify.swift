//
//  Identify.swift
//  Identify
//
//  Created by Christoffer Lundberg on 2018-12-22.
//  Copyright © 2018 christofferone. All rights reserved.
//

import Foundation

class Identify: CustomStringConvertible {
    var description: String
    let ssn: String
    
    var length: [Int] = [10, 12]
    
    init(ssn: String) {
        self.ssn = ssn.replacingOccurrences(of: "-", with: "")
        self.description = ssn
    }
    
    func validate() -> Bool {
        let length = self.ssn.count
        if !self.length.contains(length) {
            return false
        }
        
        return true
    }
}
