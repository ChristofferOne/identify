//
//  IdentifyTests.swift
//  IdentifyTests
//
//  Created by Christoffer Lundberg on 2018-12-22.
//  Copyright © 2018 christofferone. All rights reserved.
//

import XCTest
@testable import Identify

class IdentifyTests: XCTestCase {
    func test_create_swedish_number_saves() {
        let id = Identify(ssn: "950412-4335")
        
        XCTAssertEqual(id.ssn, "9504124335")
    }
    
    func test_create_swedish_number_description_saves() {
        let id = Identify(ssn: "950412-4335")
        
        XCTAssertEqual(String(describing: id), "950412-4335")
    }
    
    func test_validate_swedish_number_length_validate_returns_true() {
        let id = Identify(ssn: "950412-4335")
        
        XCTAssertTrue(id.validate())
    }
    
    func test_validate_swedish_number_wrong_length_validate_returns_false() {
        let id = Identify(ssn: "950412-433")
        
        XCTAssertFalse(id.validate())
    }
}
